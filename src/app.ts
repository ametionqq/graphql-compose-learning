import mongoose from "mongoose";
import { schema } from "./schema";
import "dotenv/config";
import { ApolloServer } from "apollo-server";

mongoose.connect(process.env.db!);

const server = new ApolloServer({ schema });

server.listen(4000, async () => {
  console.log("started");
});
