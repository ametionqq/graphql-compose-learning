import mongoose from "mongoose";

interface IUser {
  firstName: string;
  secondName: string;
  login: string;
  password: string;
} 

interface IPost {
  title: string;
  content: string;
  author: IUser;
}

const UserSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  secondName: {
    type: String,
    required: true,
  },
  login: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
});

export const UserModel = mongoose.model<IUser>("User", UserSchema);
export const PostModel = mongoose.model<IPost>("Post", PostSchema);
