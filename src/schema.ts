import { schemaComposer } from "graphql-compose";
import { composeWithMongoose } from "graphql-compose-mongoose";
import { PostModel, UserModel } from "./models";

const UserTC = composeWithMongoose(UserModel);
schemaComposer.Query.addFields({
  userById: UserTC.getResolver("findById"),
  userOne: UserTC.getResolver("findOne"),
  userMany: UserTC.getResolver("findMany"),
});

schemaComposer.Mutation.addFields({
  createUser: UserTC.getResolver("createOne"),
});

const PostTC = composeWithMongoose(PostModel);
schemaComposer.Query.addFields({
  postById: PostTC.getResolver("findById"),
  postOne: PostTC.getResolver("findOne"),
  postMany: PostTC.getResolver("findMany"),
});

schemaComposer.Mutation.addFields({
  createPost: PostTC.getResolver("createOne"),
});

PostTC.addRelation("author", {
  resolver: () => UserTC.getResolver("findById"),
  prepareArgs: {
    //@ts-ignore
    _id: (source) => source.author,
  },
  projection: { author: 1 },
});

export const schema = schemaComposer.buildSchema();
